//
//  WindowThing.swift
//  FrameworkFun
//
//  Created by Joel Rennich on 8/26/18.
//  Copyright © 2018 Joel Rennich. All rights reserved.
//

import Foundation
import Cocoa

public let FFrandomWindow = FFWindowThing(windowNibName: NSNib.Name(rawValue: "WindowThing"))

public class FFWindowThing : NSWindowController {
    
    @IBOutlet weak var button: NSButton!
    @IBOutlet weak var text: NSTextField!
    
    let doeet = FFDoEet.init()
        
    public func run() {
        updateUI()
    }
    
    func randomBits() -> String {
        
        let randText = doeet.random()
        let randTextC = doeet.randomC()
        
        // Do any additional setup after loading the view.
        return "Random bits: \n\(randText) \n\(randTextC)"
    }
    
    func updateUI() {
        
        RunLoop.main.perform {
            self.text.stringValue = self.randomBits()
        }
    }
    
    @IBAction func clickButton(_ sender: Any) {
        updateUI()
    }
    
}

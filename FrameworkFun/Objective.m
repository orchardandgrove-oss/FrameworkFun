//
//  Objective.m
//  FrameworkFun
//
//  Created by Joel Rennich on 8/24/18.
//  Copyright © 2018 Joel Rennich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Objective.h"

@interface Objective ()

@property (nonatomic, assign, readwrite) BOOL                finished;

@end

@implementation Objective

- (NSString *)getRandom;  {
    return [[NSProcessInfo processInfo] globallyUniqueString];
}

@end


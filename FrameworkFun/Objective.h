//
//  Objective.h
//  FrameworkFun
//
//  Created by Joel Rennich on 8/24/18.
//  Copyright © 2018 Joel Rennich. All rights reserved.
//

#ifndef Objective_h
#define Objective_h

#import <Foundation/Foundation.h>

@interface Objective : NSObject
@property (nonatomic, assign, readonly) BOOL                        finished;   // observable

- (NSString *)getRandom;

@end

#endif /* Objective_h */

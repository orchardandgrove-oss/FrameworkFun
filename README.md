#  Quick example of making a native Swift framework

## Creating the project

1. Very straightforward, create a new project in Xcode and choose Cocoa/Cocoa Touch/Watch/Apple TV Framework depending on what platform you'd like to start with.

2. Add code

3. Profit

## Things to keep in mind

* Keep your naming conventions clean, as you don't want to conflict with other frameworks or user code
* Use a modulemap to expose ObjC code up to the Swift parts of your framework
* Joel often smells of elderberries 

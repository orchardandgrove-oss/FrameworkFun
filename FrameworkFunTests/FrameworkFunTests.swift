//
//  FrameworkFunTests.swift
//  FrameworkFunTests
//
//  Created by Joel Rennich on 8/23/18.
//  Copyright © 2018 Joel Rennich. All rights reserved.
//

import XCTest
import FrameworkFun

class FrameworkFunTests: XCTestCase {
    
    var framework : AnyObject?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        framework = FFDoEet.init()
        
        XCTAssert(framework != nil, "Unable to instantiate.")

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRandom1() {
        let random1 = framework?.random()
        let random2 = framework?.random()
        
        XCTAssert(random1 != nil && random2 != nil, "Returning a nil instead of a random value")
        XCTAssert(random1 != random2, "Not returning random values")
    }
    
    func testRandom2() {
        let random3 = framework?.randomC()
        let random4 = framework?.randomC()
        
        XCTAssert(random3 != nil && random4 != nil, "Returning a nil instead of a random value")
        XCTAssert(random3 != random4, "Not returning random values")
    }
    
    func testWindowCreation() {
        let window = FFWindowThing(windowNibName: NSNib.Name(rawValue: "WindowThing"))
        XCTAssert(window != nil, "Unable to make window.")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            _ = framework?.random()
            _ = framework?.randomC()
        }
    }
    
}

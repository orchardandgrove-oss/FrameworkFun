//
//  DoEeet.swift
//  FrameworkFun
//
//  Created by Joel Rennich on 8/23/18.
//  Copyright © 2018 Joel Rennich. All rights reserved.
//

import Foundation
import FrameworkPrivate

@objc public class FFDoEet : NSObject {
    
    @objc public var look = "Can see me"
    private var noLook = "Can't see me"
    
    /// Returns a random string in the form of a UUID
    ///
    /// - Returns: String of random characters
    @objc public func random() -> String {
        // returns a random guid
        return UUID().uuidString
    }
    
    /// Returns a random string using via an ObjC call
    ///
    /// - Returns: String of random characters
    @objc public func randomC() -> String {
        
        let obj = Objective.init()
        return obj.getRandom()
    }
    
    /// Returns an Int, but can only be accessed inside the framework
    ///
    /// - Returns: Int
    private func cantTouchThis() -> Int {
        return 1
    }
}
